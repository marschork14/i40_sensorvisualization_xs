/**
    * Method for for checking the referential constraint 
    * between the sensor and a machine entity
    * @param {object} oSensor object which should be checked
	* @returns {boolean} indicates if the referential contraint is valid
*/
function checkSensorIntegrity(oSensor) {
	
	var oConn = $.hdb.getConnection(); 
	var iMachineID = parseInt(oSensor["machine.machineID"],10);
	var sQuery = 'SELECT * FROM "DEVELOPER"."lehre.mosbach.dhbw.visualization.model::model.machine" WHERE "machineID" = ?';
	var oResult = oConn.executeQuery(sQuery, iMachineID);
	oConn.close();
	
	return oResult["0"] !== undefined;
}

/**
    * Method for validating the JSON input of a POST call
    * @param {object} oSensor object to validate
	* @returns {boolean} indicates if the object is valid
*/
function validateSensorPost(oSensor) {
	
	var bName = oSensor.name !== undefined ? true : false;
	var bTechnicalName = oSensor.technicalName !== undefined ? true : false;
	var bSensorType = oSensor.sensorType !== undefined ? true : false;
	var bUnit = oSensor.unit !== undefined ? true : false;
	var bRefreshTime = oSensor.refreshTime !== undefined ? true : false;
	var bThresholdLow = oSensor.thresholdLow !== undefined ? true : false;
	var bThresholdHigh = oSensor.thresholdHigh !== undefined ? true : false;
	var bThresholdExceeded = oSensor.thresholdExceeded !== undefined ? true : false;
	var bCheckThresholdMail = oSensor.checkThresholdMail !== undefined ? true : false;
	var bCheckThresholdTwitter = oSensor.checkThresholdTwitter !== undefined ? true : false;
	var bEmail = oSensor.email !== undefined ? true : false;
	var bMachineID = oSensor["machine.machineID"] !== undefined ? true : false;
	var bActive = oSensor.active !== undefined ? true : false;
	
	return bName && bTechnicalName && bSensorType && bUnit && 
		   bRefreshTime && bThresholdLow && bThresholdHigh && 
	       bThresholdExceeded &&  bCheckThresholdMail && 
	       bCheckThresholdTwitter && bEmail && bMachineID && bActive;
}

/**
    * Method for validating the JSON input of a PUT call
    * @param {object} oSensor object to validate
	* @returns {boolean} indicates if the object is valid
*/
function validateSensorPut(oSensor) {
	
	var bValidSensorID = false;
	if (oSensor.sensorID !== undefined) {
	    bValidSensorID = !isNaN(parseInt(oSensor.sensorID,10));
	}
	
	return validateSensorPost(oSensor) && bValidSensorID;
}

/**
    * Method for creating the response of a GET call 
*/
function getSensor(machineID) {
	
	var oConn = $.hdb.getConnection();
	var sQuery = 'SELECT * FROM "DEVELOPER"."lehre.mosbach.dhbw.visualization.model::model.sensor"' +
				 'WHERE "machine.machineID" = ?';
	var oResult = oConn.executeQuery(sQuery, machineID);
	var aResult = [];
	var prop;
	for (prop in oResult) {
		if(oResult[prop] !== undefined) {
			aResult.push(oResult[prop]);
		}
	}
	$.response.status = $.net.http.OK;
	$.response.setBody(JSON.stringify(aResult));
	oConn.close();
	return;
}

/**
    * Method for inserting a new sensor and creating the http response
    * with the created sensor object
    * Sets an error message if the input is invalid
    * @param {object} oSensor object to insert
*/
function postSensor(oSensor) {
	
	if(validateSensorPost(oSensor) && checkSensorIntegrity(oSensor)) {
		var oConn = $.hdb.getConnection();
		//new unique id from database sequence
		var sQuery = 'SELECT "DEVELOPER"."lehre.mosbach.dhbw.visualization::sequence".NEXTVAL FROM dummy';
		var oResult = JSON.parse(JSON.stringify(oConn.executeQuery(sQuery)))["0"];
		var iSensorID = parseInt(oResult["lehre.mosbach.dhbw.visualization::sequence.NEXTVAL"],10);
		sQuery = 'INSERT INTO "DEVELOPER"."lehre.mosbach.dhbw.visualization.model::model.sensor" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
		oConn.executeUpdate(sQuery, iSensorID, oSensor.name, oSensor.technicalName, 
							oSensor.sensorType, oSensor.unit, oSensor.refreshTime,
							oSensor.thresholdLow, oSensor.thresholdHigh,
							oSensor.thresholdExceeded, oSensor.checkThresholdMail, 
							oSensor.checkThresholdTwitter, oSensor.email, oSensor.active, parseInt(oSensor["machine.machineID"],10));
		oConn.commit();
		
		sQuery = 'SELECT * FROM "DEVELOPER"."lehre.mosbach.dhbw.visualization.model::model.sensor" WHERE "sensorID" = ?';
		oResult = oConn.executeQuery(sQuery, iSensorID);
		$.response.status = $.net.http.CREATED;
		$.response.setBody(JSON.stringify({sensor: oResult["0"]}));
		oConn.close();
		
	} else {
		
		$.response.status = $.net.http.BAD_REQUEST;
		$.response.setBody(JSON.stringify({errorCode: $.net.http.BAD_REQUEST, errorMessage: "Invalid input!"}));
	}
    return;
}

/**
    * Method for updating an existing machine
    * Sets an error message if the input is invalid
    * @param {object} oSensor object to update
*/
function updateSensor(oSensor) {
	
	if(validateSensorPut(oSensor) && checkSensorIntegrity(oSensor)) {
		
		var oConn = $.hdb.getConnection();
		var sQuery = 'UPDATE "DEVELOPER"."lehre.mosbach.dhbw.visualization.model::model.sensor" SET "name" = ?, "technicalName" = ?,' +
					 ' "sensorType" = ?, "unit" = ?, "refreshTime" = ?, "thresholdLow" = ?, "thresholdHigh" = ?,' +
					 ' "thresholdExceeded" = ?, "checkThresholdMail" = ?,' +
					 ' "checkThresholdTwitter" = ?, "email" = ?, "active" = ?, "machine.machineID" = ? WHERE "sensorID" = ?';
		
		oConn.executeUpdate(sQuery, oSensor.name, oSensor.technicalName, 
							oSensor.sensorType, oSensor.unit, oSensor.refreshTime,
							oSensor.thresholdLow, oSensor.thresholdHigh,
							oSensor.thresholdExceeded, oSensor.checkThresholdMail, 
							oSensor.checkThresholdTwitter, oSensor.email, oSensor.active, 
							parseInt(oSensor["machine.machineID"],10), parseInt(oSensor.sensorID, 10));
		oConn.commit();
		oConn.close();
		$.response.status = $.net.http.NO_CONTENT;
		
	} else {
		
		$.response.status = $.net.http.BAD_REQUEST;
		$.response.setBody(JSON.stringify({errorCode: $.net.http.BAD_REQUEST, errorMessage: "Invalid input!"}));
	}
	return;
}

/**
    * Method for deleting an existing sensor
    * Sets an error message if the input is invalid
    * @param {string} sSensorID id of the sensor which should be deleted
*/
function deleteSensor(sSensorID) {
	
	var iSensorID = parseInt(sSensorID, 10);
	
	if(!isNaN(iSensorID)) {
		var oConn = $.hdb.getConnection();
		var sQuery = 'DELETE FROM "DEVELOPER"."lehre.mosbach.dhbw.visualization.model::model.sensor" WHERE "sensorID" = ?';
		oConn.executeUpdate(sQuery, iSensorID);
		oConn.commit();
		oConn.close();
		$.response.status = $.net.http.NO_CONTENT;	
		
	} else {
		
		$.response.status = $.net.http.NOT_FOUND;
		$.response.setBody(JSON.stringify({errorCode: $.net.http.NOT_FOUND, errorMessage: "Resource not found!"}));
	}
    return;
}