/**
    * Method for validating the JSON input of a POST call
    * @param {object} oMachine object to validate
	* @returns {boolean} indicates if the object is valid
*/
function validateMachinePost(oMachine) {
	
	var bName = oMachine.name !== undefined ? true : false;
	var bActive =  oMachine.active !== undefined ? true : false;
	
	return bName && bActive;
}

/**
    * Method for validating the JSON input of a PUT call
    * @param {object} oMachine object to validate
	* @returns {boolean} indicates if the object is valid
*/
function validateMachinePut(oMachine) {
	
	var bValidMachineID = false;
	if (oMachine.machineID !== undefined) {
	    bValidMachineID = !isNaN(parseInt(oMachine.machineID,10));
	}
	
	return validateMachinePost(oMachine) && bValidMachineID;
}

/**
    * Method for creating the response of a GET call 
*/
function getMachine() {
	
	var oConn = $.hdb.getConnection();
	var sQuery = 'SELECT * FROM "DEVELOPER"."lehre.mosbach.dhbw.visualization.model::model.machine"';
	var oResult = oConn.executeQuery(sQuery);
	var aResult = [];
	var prop;
	for (prop in oResult) {
		if(oResult[prop] !== undefined) {
			aResult.push(oResult[prop]);
		}
	}
	$.response.status = $.net.http.OK;
	$.response.setBody(JSON.stringify(aResult));
	oConn.close();
	return;
}

/**
    * Method for inserting a new machine and creating the http response
    * with the created machine object
    * Sets an error message if the input is invalid
    * @param {object} oMachine object to insert
*/
function postMachine(oMachine) {
	
	if(validateMachinePost(oMachine)) {
	    
		var oConn = $.hdb.getConnection();
		//new unique id from database sequence
		var sQuery = 'SELECT "DEVELOPER"."lehre.mosbach.dhbw.visualization::sequence".NEXTVAL FROM dummy';
		var oResult = JSON.parse(JSON.stringify(oConn.executeQuery(sQuery)))["0"];
		var iMachineID = parseInt(oResult["lehre.mosbach.dhbw.visualization::sequence.NEXTVAL"],10);
		//insert new machine
		sQuery = 'INSERT INTO "DEVELOPER"."lehre.mosbach.dhbw.visualization.model::model.machine" VALUES (?,?,?)';
		oResult = oConn.executeUpdate(sQuery, iMachineID, oMachine.name, oMachine.active);
		oConn.commit();
		//read inserted machine and send to user
		sQuery = 'SELECT * FROM "DEVELOPER"."lehre.mosbach.dhbw.visualization.model::model.machine" WHERE "machineID" = ?';
		oResult = oConn.executeQuery(sQuery, iMachineID);
		$.response.status = $.net.http.CREATED;
		$.response.setBody(JSON.stringify({machine: oResult["0"]}));
		oConn.close();
	
	} else {
		
		$.response.status = $.net.http.BAD_REQUEST;
		$.response.setBody(JSON.stringify({errorCode: $.net.http.BAD_REQUEST, errorMessage: "Invalid input!"}));
	}
    return;
}

/**
    * Method for updating an existing machine
    * Sets an error message if the input is invalid
    * @param {object} oMachine object to update
*/
function updateMachine(oMachine) {
	
	if(validateMachinePut(oMachine)) {
	    
		var oConn = $.hdb.getConnection();
		var sQuery = 'UPDATE "DEVELOPER"."lehre.mosbach.dhbw.visualization.model::model.machine" SET "name" = ?, "active" = ? WHERE "machineID" = ?';
		oConn.executeUpdate(sQuery, oMachine.name, oMachine.active, parseInt(oMachine.machineID,10));
		oConn.commit();
		oConn.close();
		$.response.status = $.net.http.NO_CONTENT;
	
	} else {
		
		$.response.status = $.net.http.BAD_REQUEST;
		$.response.setBody(JSON.stringify({errorCode: $.net.http.BAD_REQUEST, errorMessage: "Invalid input!"}));
	}
	return;
}

/**
    * Method for deleting an existing machine
    * Sets an error message if the input is invalid
    * @param {string} sMachineID id of the machine which should be deleted
*/
function deleteMachine(sMachineID) {
	
	var iMachineID = parseInt(sMachineID, 10);
	
	if(!isNaN(iMachineID)) {
	    
		var oConn = $.hdb.getConnection();
		var sQuery = 'DELETE FROM "DEVELOPER"."lehre.mosbach.dhbw.visualization.model::model.machine" WHERE "machineID" = ?';
		oConn.executeUpdate(sQuery, iMachineID);
		oConn.commit();
		oConn.close();
		$.response.status = $.net.http.NO_CONTENT;
		
	} else {
		
		$.response.status = $.net.http.NOT_FOUND;
		$.response.setBody(JSON.stringify({errorCode: $.net.http.NOT_FOUND, errorMessage: "Resource not found!"}));
	}
    return;
}