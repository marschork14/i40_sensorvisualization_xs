$.import("lehre.mosbach.dhbw.visualization.lib", "Machine");
$.import("lehre.mosbach.dhbw.visualization.lib", "Sensor");

/**
    * Method for setting a error message with status
     "NOT_FOUND" in the http body
*/
function resourceNotFound() {
	 
	 $.response.status = $.net.http.NOT_FOUND;
	 $.response.setBody(JSON.stringify({errorCode:  $.net.http.NOT_FOUND,
										errorMessage: "Resource not found!"}));	
	 return;
}

/**
    * Method for extracting the query path from the url
	* @returns {string} query path in uppercase
*/
function extractQueryPathFromURL() {
	
	var sQueryPath = $.request.queryPath;
	//cut off last "/"
	if(sQueryPath.endsWith("/")) {
		sQueryPath = sQueryPath.substr(0,sQueryPath.length - 1); 
	}
	
	return sQueryPath.toUpperCase();
}

/**
    * Method for checking the input of the request body
    * Sets an error message if the body is missing or the content type is wrong
	* @returns {boolean} indicates if input is valid 
*/
function validateInput() {
	
	var sContentType = $.request.contentType;
	//content type has to be 'application/json'
	if ( sContentType === null || sContentType.startsWith("application/json") === false){
		 $.response.status = $.net.http.BAD_REQUEST;
		 $.response.setBody(JSON.stringify({errorCode:  $.net.http.BAD_REQUEST,
			   								errorMessage: "Wrong content type. Use 'application/json' instead!"}));
		return false;
	}
	
	var sBodyStr = $.request.body ? $.request.body.asString() : undefined;
	if ( sBodyStr === undefined ){
		 $.response.status = $.net.http.BAD_REQUEST;
		 $.response.setBody(JSON.stringify({errorCode:  $.net.http.BAD_REQUEST,
											errorMessage: "Request body is missing!"}));
		 return false;
	}
	
	return true;
}

/**
    * Method for delegating GET requests to the corresponding library functions
    * Sets an error message if the query path is not 'MACHINE' or 'SENSOR'
    * An error message is also set, if an url parameter is missing or of the wrong format
*/
function handleGet() {
	 
	var sMachinePath = "MACHINE";
	var sSensorPath = "SENSOR";
	
	switch (extractQueryPathFromURL()) {
	 
		case sMachinePath:
			$.lehre.mosbach.dhbw.visualization.lib.Machine.getMachine();
		    break;
         
		case sSensorPath:
			var sMachineID = $.request.parameters.get("machineID");
			if (sMachineID === undefined) {
				$.response.status = $.net.http.BAD_REQUEST;
				$.response.setBody(JSON.stringify({errorCode:  $.net.http.BAD_REQUEST,
					   							   errorMessage: "Missing URL parameter (machineID)!"}));	
			} else {
				var iMachineID = parseInt(sMachineID, 10);
				if(isNaN(iMachineID)) {
					$.response.status = $.net.http.BAD_REQUEST;
					$.response.setBody(JSON.stringify({errorCode:  $.net.http.BAD_REQUEST,
						   							   errorMessage: "Invalid type of URL parameter!"}));	
				} else {
					$.lehre.mosbach.dhbw.visualization.lib.Sensor.getSensor(iMachineID);
				}
			}
		    break; 
		
		default:
			resourceNotFound();
	 }
	 
	 return;
}

/**
    * Method for delegating POST requests to the corresponding library functions
    * Sets an error message if input can not be parsed
*/
function handlePost() {
	
	if(validateInput()) {

		var sMachinePath = "MACHINE";
		var sSensorPath = "SENSOR";
		
		var oObject = {};
		try {
			
			oObject = JSON.parse($.request.body.asString());	
		
		} catch(error) {
			
			$.response.status = $.net.http.BAD_REQUEST;
			$.response.setBody(JSON.stringify({errorCode: $.net.http.BAD_REQUEST,
											   errorMessage: "Invalid input!"}));
		  return;
		}
		
		switch (extractQueryPathFromURL()) {
		 
			case sMachinePath:
				$.lehre.mosbach.dhbw.visualization.lib.Machine.postMachine(oObject);
			    break;
	         
			case sSensorPath:
				$.lehre.mosbach.dhbw.visualization.lib.Sensor.postSensor(oObject);
			    break;
			
			default:
			   resourceNotFound();	        		            
		 }
	}
	
	return;
}

/**
    * Method for delegating PUT requests to the corresponding library functions
    * Sets an error message if input can not be parsed
*/
function handlePut() {
	
	var sMachinePath = "MACHINE";
	var sSensorPath = "SENSOR";
	
	if(validateInput()) {
	    
    	var oObject = {};
    	try {
    		
    		oObject = JSON.parse($.request.body.asString());	
    	
    	} catch(error) {
    		
    		$.response.status = $.net.http.BAD_REQUEST;
    		$.response.setBody(JSON.stringify({errorCode: $.net.http.BAD_REQUEST,
    										   errorMessage: "Invalid input!"}));
    		return;
    	}
    	
    	switch (extractQueryPathFromURL()) {
    	 
    		case sMachinePath:
    			$.lehre.mosbach.dhbw.visualization.lib.Machine.updateMachine(oObject);
    		    break;
             
    		case sSensorPath:
    			$.lehre.mosbach.dhbw.visualization.lib.Sensor.updateSensor(oObject);
    		    break; 
    		
    		default:
    			resourceNotFound();	        		              
    	}
	}
	return;
}

/**
    * Method for delegating DELETE requests to the corresponding library functions
*/
function handleDelete() {

	var sMachinePath = "MACHINE";
	var sSensorPath = "SENSOR";
	
	var aString = extractQueryPathFromURL().split("/");
	
	switch (aString[0]) {
	 
		case sMachinePath:
			$.lehre.mosbach.dhbw.visualization.lib.Machine.deleteMachine(aString[1]);
		    break;
         
		case sSensorPath:
			$.lehre.mosbach.dhbw.visualization.lib.Sensor.deleteSensor(aString[1]);
		    break; 
		
		default:
		    resourceNotFound();	        		            
	 }
	
	return;
}

/**
    * Method for handling the incoming http request
    * Delegates processing due the http verb 
    * Sets an error message if the http method differs from GET, PUT, POST or DELETE
*/
function processRequest(){
	
    switch ( $.request.method ) {
    
        case $.net.http.GET:
            handleGet();
            break;
     
        case $.net.http.POST:
           handlePost();
           break; 
       
        case $.net.http.PUT:
        	handlePut();
        	break;
        
        case 5:
        	handleDelete();
        	break;
        
        default:
            $.response.status = $.net.http.METHOD_NOT_ALLOWED;
            $.response.setBody(JSON.stringify({errorCode:  $.net.http.METHOD_NOT_ALLOWED,
            								   errorMessage: "Wrong HTTP method. Only GET," +
            									    		 "POST, PUT and DELETE are allowed!"}));
        	break;
    }
    $.response.contentType = "application/json";
}

// Call request processing  
processRequest();